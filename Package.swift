// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
  name: "swsgi",
  products: [
    // Products define the executables and libraries a package produces, and make them visible to other packages.
    .library(
      name: "swsgi",
      targets: ["swsgi"]),
    .executable(name: "swcgi", targets: ["swcgi"]),
    .executable(name: "swnioserver", targets: ["swnioserver"])
  ],
  dependencies: [
    // Dependencies declare other packages that this package depends on.
    // .package(url: /* package url */, from: "1.0.0"),
    .package(name: "swift-nio", url: "https://github.com/apple/swift-nio", from: "2.0.0")
  ],
  targets: [
    // Targets are the basic building blocks of a package. A target can define a module or a test suite.
    // Targets can depend on other targets in this package, and on products in packages this package depends on.
    .target(
      name: "swsgi",
      dependencies: []),
    .testTarget(
      name: "swsgiTests",
      dependencies: ["swsgi"]),
    .target(name: "swcgi", dependencies: ["swsgi"]),
    .target(name: "swnioserver", dependencies: ["swsgi", .product(name: "NIO", package: "swift-nio"), .product(name: "NIOHTTP1", package: "swift-nio")])
  ]

)
