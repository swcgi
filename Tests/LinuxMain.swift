import XCTest
import swsgiTests

var tests = [XCTestCaseEntry]()
tests += swsgiTests.allTests()
XCTMain(tests)
