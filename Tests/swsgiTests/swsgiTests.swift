import XCTest

@testable import swsgi

final class swsgiTests: XCTestCase {
  func testExample() {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct
    // results.
    XCTAssertEqual(swsgi().text, "Hello, World!")
  }

  static var allTests = [
    ("testExample", testExample)
  ]
}
