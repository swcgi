import NIO
import NIOHTTP1

open class Express: Router {
  let loopGroup = MultiThreadedEventLoopGroup(numberOfThreads: System.coreCount)
  final class HTTPHandler: ChannelInboundHandler {
    typealias InboundIn = HTTPServerRequestPart

    let router: Router
    init(router: Router) {
      self.router = router
    }

    func channelRead(context: ChannelHandlerContext, data: NIOAny) {
      let reqPart = unwrapInboundIn(data)
      switch reqPart {
      case .head(let header):
        let request = SWRequest(header: header)
        let response = SWResponse(channel: context.channel)

        router.handle(request: request, response: response) {
          (items: Any...) in
          response.status = .notFound
          response.send("Nothing handled the request!")
        }

      // ignore incoming content...
      case .body, .end: break
      }

    }
  }

  open func listen(_ port: Int) {
    let reuseAddrOpt = ChannelOptions.socket(SocketOptionLevel(SOL_SOCKET), SO_REUSEADDR)
    let bootstrap = ServerBootstrap(group: loopGroup)
      .serverChannelOption(ChannelOptions.backlog, value: 256)
      .serverChannelOption(reuseAddrOpt, value: 1)
      .childChannelInitializer({ channel in
        return channel.pipeline.configureHTTPServerPipeline(withErrorHandling: true).flatMap {
          channel.pipeline.addHandler(HTTPHandler(router: self))
        }
      })
      .childChannelOption(ChannelOptions.socket(IPPROTO_TCP, TCP_NODELAY), value: 1)
      .childChannelOption(reuseAddrOpt, value: 1)
      .childChannelOption(ChannelOptions.maxMessagesPerRead, value: 1)

    do {
      let serverChannel = try bootstrap.bind(host: "localhost", port: port).wait()
      print("Server running on:", serverChannel.localAddress!)
      try serverChannel.closeFuture.wait()
    } catch {
      fatalError("Failed to start server: \(error)")
    }
  }
}
