import NIOHTTP1

open class SWRequest {
  public let header: HTTPRequestHead
  public var userInfo = [String: Any]()
  init(header: HTTPRequestHead) {
    self.header = header
  }
}
