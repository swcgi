open class Router {
  private var middleware = [Middleware]()

  open func use(_ middleware: Middleware...) {
    self.middleware.append(contentsOf: middleware)
  }

  func handle(request: SWRequest, response: SWResponse, next upperNext: @escaping Next) {
    let stack = self.middleware
    guard !stack.isEmpty else { return upperNext() }

    var next: Next? = { (args: Any...) in }
    var i = stack.startIndex
    next = { (args: Any...) in
      let middleware = stack[i]
      i = stack.index(after: i)
      middleware(request, response, i == stack.endIndex ? upperNext : next!)
    }
    next!()
  }
}

extension Router {
  public func get(_ path: String = "", middleware: @escaping Middleware) {
    use { req, res, next in
      guard req.header.method == .GET, req.header.uri.hasPrefix(path) else { return next() }
      middleware(req, res, next)
    }
  }
}
