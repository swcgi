public typealias Next = (Any...) -> Void
public typealias Middleware = (SWRequest, SWResponse, @escaping Next) -> Void
