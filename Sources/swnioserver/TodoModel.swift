struct Todo: Codable {
  var id: Int
  var title: String
  var completed: Bool
}

let TODOS = [
  Todo(id: 42, title: "Gay stuff", completed: false),
  Todo(id: 43, title: "More gay stuff", completed: false),
  Todo(id: 45, title: "The gay agenda", completed: true),
]
