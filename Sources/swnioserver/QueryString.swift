import Foundation

private let paramDictKey = "io.github.cdcarter.swiftymu.param"

public func querystring(req: SWRequest, res: SWResponse, next: @escaping Next) {
  if let queryItems = URLComponents(string: req.header.uri)?.queryItems {
    req.userInfo[paramDictKey] = Dictionary(grouping: queryItems, by: { $0.name }).mapValues {
      $0.compactMap({ $0.value }).joined(separator: ",")
    }
  }
  next()
}

extension SWRequest {
  public func param(_ id: String) -> String? {
    return (userInfo[paramDictKey] as? [String: String])?[id]
  }
}
