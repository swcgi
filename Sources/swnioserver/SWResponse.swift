import NIO
import NIOHTTP1

import struct Foundation.Data
import class Foundation.JSONEncoder

open class SWResponse {
  public var status = HTTPResponseStatus.ok
  public var headers = HTTPHeaders()
  public let channel: Channel
  private var didWriteHeader = false
  private var didEnd = false

  public init(channel: Channel) {
    self.channel = channel
  }

  open func send(_ s: String) {
    flushHeader()

    var buffer = channel.allocator.buffer(capacity: s.count)
    buffer.writeString(s)

    let part = HTTPServerResponsePart.body(.byteBuffer(buffer))
    _ = channel.writeAndFlush(part).recover(handleError).map(end)
  }

  func flushHeader() {
    guard !didWriteHeader else { return }
    didWriteHeader = true
    let head = HTTPResponseHead(
      version: .init(major: 1, minor: 1), status: status, headers: headers)
    let part = HTTPServerResponsePart.head(head)
    _ = channel.writeAndFlush(part).recover(handleError)
  }

  func handleError(_ error: Error) {
    print("ERROR:", error)
    end()
  }
  func end() {
    guard !didEnd else { return }
    didEnd = true
    _ = channel.writeAndFlush(HTTPServerResponsePart.end(nil)).map { self.channel.close() }
  }
}

extension SWResponse {
  public subscript(name: String) -> String? {
    set {
      assert(!didWriteHeader, "already wrote header!")
      if let v = newValue {
        headers.replaceOrAdd(name: name, value: v)
      } else {
        headers.remove(name: name)
      }
    }
    get {
      return headers[name].joined(separator: ", ")
    }
  }
}

extension SWResponse {
  public func json<T: Encodable>(_ model: T) {
    let data: Data
    do {
      data = try JSONEncoder().encode(model)
    } catch {
      return handleError(error)
    }

    self["Content-Type"] = "application/json"
    self["Content-Length"] = "\(data.count)"
    flushHeader()

    var buffer = channel.allocator.buffer(capacity: data.count)
    buffer.writeBytes(data)
    let part = HTTPServerResponsePart.body(.byteBuffer(buffer))
    _ = channel.writeAndFlush(part).recover(handleError).map(end)
  }
}
