let app = Express()

app.use(querystring, cors(allowOrigin: "*"))
app.use { req, res, next in
  print("\(req.header.method): ", req.header.uri)
  next()
}

app.get("/hello") { req, res, _ in
  let you = req.param("who") ?? " you"
  res.send(" hello to \(you)")
}

app.get("/woof") { _, res, _ in
  res.send(" woof")
}

app.get("/todomvc") { _, res, _ in
  res.json(TODOS)
}

app.listen(1337)
