import Foundation

public struct Request {
  public var headers: HTTPHeaders
  public var method: HTTPMethod
  public var path: String

  public init(headers: HTTPHeaders, method: HTTPMethod, path: String) {
    self.headers = headers
    self.method = method
    self.path = path
  }
}

extension Request {
  public static func fromEnv() -> Request {
    var headers = HTTPHeaders()
    let path = ProcessInfo.processInfo.environment["SCRIPT_NAME"] ?? ""
    let method = HTTPMethod.fromString(
      ProcessInfo.processInfo.environment["REQUEST_METHOD"] ?? "get")
    for (name, value) in ProcessInfo.processInfo.environment {
      if name.starts(with: "HTTP") {
        headers.add(name: name, value: value)
      }
    }
    return Request(headers: headers, method: method, path: path)
  }
}
