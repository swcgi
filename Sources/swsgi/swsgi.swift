import Foundation

public typealias APP = (
  [String: Any],
  @escaping ((String, [(String, String)]) -> Void),
  @escaping ((Data) -> Void)
) -> Void
