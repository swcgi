SWIFTCMD=swift
SWIFTBUILD=$(SWIFTCMD) build
SWIFTCLEAN=$(SWIFTCMD) package clean
SWIFTTEST=$(SWIFTCMD) test
SWIFTRESET=$(SWIFTCMD) package reset
SWIFTRESOLVE=$(SWIFTCMD) package resolve

# todo dist, install
# learn how the build output goes...


.PHONY: build
build :
	$(SWIFTBUILD)

.PHONY: test
test :
	$(SWIFTTEST)

.PHONY: clean
clean :
	$(SWIFTCLEAN)

.PHONY: distclean
distclean :
	$(SWIFTRESET)

.PHONY: deps
deps :
	$(SWIFTRESOLVE)

#https://stackoverflow.com/a/26339924/5042831
.PHONY: list
list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'
