# SWSGI

A set of tools and building blocks for responding to HTTP requests from Swift.

Roughly based on SWSGI from [envoy/Embassy]( 
https://github.com/envoy/Embassy#whats-swsgi-swift-web-server-gateway-interface)

## Next Steps
[] unify SWNIOServer with the swsgi data structures
[] add more swsgi data structures based on golang and wsgi
[] swcgi connect it up

# Target Layout

## swsgi

The main building blocks for generically refering to HTTP requests/responses (and their 
components). Additionally defines the swsgi/app type.

Roughly based on SWSGI from [envoy/Embassy]( 
https://github.com/envoy/Embassy#whats-swsgi-swift-web-server-gateway-interface)
with additional types and inspiration from [allevato/SwiftCGI](https://github.com/allevato/SwiftCGI/)
and
[Alamofire](https://github.com/Alamofire/Alamofire).

## swcgi

Tools for writing CGI binaries (that respond to a single request via stdin/stdout and
environment variables). Optionally hosting a SWSGI app.

This currently builds as an executable, but someday should probably be a library with
a sample executable. 

## swnioserver

Uses Swift-NIO to run a (persistent) HTTP server that stays resident and responds
to many requests. Optionally hosting a SWSGI app.

Based on the uExpress exampel at https://www.alwaysrightinstitute.com/microexpress-nio2/
by https://github.com/helje5.

# Project TODOs (non-functional-reqs)
[] Setup swift-doc
[] Setup swift-format auto
[] Setup test harness